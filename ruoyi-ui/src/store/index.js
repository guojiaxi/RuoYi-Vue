import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import tagsView from './modules/tagsView'
import permission from './modules/permission'
import settings from './modules/settings'
import getters from './getters'

// 一般我们不直接在main.js中去引入vuex对象
Vue.use(Vuex)

// 创建的是Vuex.Store对象
const store = new Vuex.Store({
  // 共享属性是挂载到state对象中
  // state:{
  //   couter:1000
  // },
  modules: {
    app,
    user,
    tagsView,
    permission,
    settings
  },
  getters
})

export default store
