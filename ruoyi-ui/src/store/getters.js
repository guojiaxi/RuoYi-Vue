// 当我们希望获取到经过某些处理后的state中的参数（类似于sql语句对数据进行处理，和vue中的computed）就能使用getter
const getters = {
  // getter中属性都有一个默认参数state，属性的写法类似于vue中的computed
  // getters中属性也能做雷士计算属性的计算操作
  // more20stu: state => state.students.filter(stu => stu.age > 20)
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permissions: state => state.user.permissions,
  permission_routes: state => state.permission.routes,
  topbarRouters:state => state.permission.topbarRouters,
  defaultRoutes:state => state.permission.defaultRoutes,
  sidebarRouters:state => state.permission.sidebarRouters,
}
export default getters
